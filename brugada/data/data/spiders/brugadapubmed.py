import scrapy
from scrapy.spiders import XMLFeedSpider

id = []

class Brugadapubmed(XMLFeedSpider):
    name = "brugadapubmed"

    start_urls = [
        'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pubmed&term=brugada+syndrome+review&usehistory=y&retmax=657',
    ]
    def parse(self, response):
        idss = ""
        for link in response.selector.xpath('.//Id/text()').extract():
            id.append(link)
            idss +=str(link)+','

        url = "https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?"+'db=pubmed'+'&id='+idss[:len(idss)-1]+'&retmode=xml'
        return scrapy.Request(url,callback=self.parse_page2,method='GET')

    def parse_page2(self, response):
        print('In parse method')
        for link in response.selector.xpath('.//PubmedArticle'):
             yield {
                 'Article Id': link.xpath('.//PMID/text()').extract(),
                 'Article Title':link.xpath('.//Article//ArticleTitle/text()').extract(),
                 'Author': link.xpath('.//Article//ArticleTitle//AuthorList//Author//ForeName/text()').extract(),
                 'Abstract':link.xpath('.//Article//Abstract//AbstractText/text()').extract(),
             }