import scrapy


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    start_urls = [
        'https://www.ncbi.nlm.nih.gov/books/NBK1517/#brugada.molgen.TA',
    ]

    def parse(self, response):
        A = response.xpath('//div[@id="__brugada.molgen.TA_lrgtbl__"]')
        for row in A.xpath('table/tbody/tr'):
             gene = row.xpath('td[1]/a/i/text()').extract_first()
             gene_link = row.xpath('td[1]/a/@href').extract_first()
             chromosome = row.xpath('td[2]/a/text()').extract_first()
             chromosome_link = row.xpath('td[2]/a/@href').extract_first()
             protein = row.xpath('td[3]/a/text()').extract_first()
             protein_link = row.xpath('td[3]/a/@href').extract_first()
             locous_specific = row.xpath('td[4]/a/text()').extract_first()
             hgdm = row.xpath('td[5]/a/text()').extract_first()
             hgdm_link = row.xpath('td[5]/a/@href').extract_first()
             yield {
                 'gene':gene,
                 'gene_link':'https://www.ncbi.nlm.nih.gov' + str(gene_link),
                 'chromosome':chromosome,
                 'chromosome_link':chromosome_link,
                 'protein':protein,
                 'protein_link':protein_link,
                 'locous':locous_specific,
                 'hgdm':hgdm,
                 'hgdm_link':hgdm_link
              }
