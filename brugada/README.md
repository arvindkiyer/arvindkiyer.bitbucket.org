# Welcome to Project #

### What is this repository for? ###

* A course work project on Brugada Syndrome.

### How do I get set up? ###

* First install the requirements.txt in your virtual environment 
```
#!python
source bin/activate
pip3 install requirements.txt
```

### Contribution guidelines ###

* To be added soon.

### Who do I talk to? ###

* [Arvind Iyer](https://bitbucket.org/arvindkiyer/)