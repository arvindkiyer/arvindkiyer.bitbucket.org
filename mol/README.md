Molviewer- Computer Graphics Project

## Installation
Run the index.html page.

## External Libaries
Three.js, Jquery, Stat.js,dat.gui.js

## Credits

- [@arvind](https://github.com/Arvindiyer)
- http://wwwtyro.github.io/speck/
- Stackoverflow
- Threejs Documentation
- Learnopengl
- Arose Bitbucket notes
- Other visulization softwares

## License

[MIT License](https://github.com/Arvindiyer/molviewer/blob/master/LICENSE)

## Erors:

If get Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, https.

Run  python -m http.server [Need to have httpserver module]

You can view the page in online at [link](https://arvindkiyer.bitbucket.io/mol)
