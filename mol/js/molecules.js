// Defining Global Variables
var scence_div;
// Defining the camera,scene and Renderedobject.
var camera, scene, renererobject;
// Mouse/Key Board Controls [using Trackball.js files functions]
var controls;
// rendring material 
var material;
//variables
var root, helper;
var atomCount = 0;
// default directory of pdb files
var dir = "assets/"
var MOLECULES = {
  Testosterone: 'testosterone.pdb',
  Porin: "porin.pdb",
  "3QE5": '3QE5.pdb'
};

// Gui.Dat parameters
var guiParams = {
  Molecule: Object.keys(MOLECULES)[0],
  Saturation: 0.5,
  cutOff: 0.1,
  Samples: 80
}

var min, max, avg;

/** 
  Shader variables to load the
*/
var vertexShaders       = $('script[type="x-shader/x-vertex"]');
var fragmentShaders     = $('script[type="x-shader/x-fragment"]');
var shadersLoaderCount  = vertexShaders.length + fragmentShaders.length;

var shaderSourceCodes = {};

var loader = new THREE.Loader();

/** 
  Initializes the WebGL components and loads the first molecule.
*/
function init() {

  renererobject = new THREE.WebGLRenderer({antialias: false});
  renererobject.setPixelRatio( window.devicePixelRatio );
  renererobject.setSize( window.innerWidth, window.innerHeight );
  renererobject.setClearColor( 0xaaaaaa );
  renererobject.autoClear = true;

  if ( renererobject.extensions.get( 'ANGLE_instanced_arrays' ) === false ||
  renererobject.extensions.get('EXT_frag_depth') === false) {
    console.log('Not supported');
    return false;
  }

  scence_div = document.getElementById('scene');
  scence_div.appendChild( renererobject.domElement );

  // Setting up the camera
  camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 1, 5000 );
  camera.position.z = 1000;

  //Creating a scene and adding a camera 
  scene = new THREE.Scene();
  scene.add(camera);
  // Regestering the Controls of mouse
  controls = new THREE.TrackballControls(camera, renererobject.domElement );
  
  //Defining the material for the shader where we have lot of uniform variables and defining which shader and fragment shader to
  material = new THREE.RawShaderMaterial( {
    uniforms: {
      AOTexture: {type: 't', value: 0},
      saturation: {type: 'f', value: 1 - guiParams.Saturation},
      cutOff: {type: 'f', value: guiParams.cutOff},
      // AO: {type: 'b', value: guiParams.AO },
    },
    vertexShader: shaderSourceCodes.basic.vertex,
    fragmentShader: shaderSourceCodes.basic.fragment,
    depthTest: true,
    depthWrite: true
  } );

  root = new THREE.Group();
  helper = new THREE.Group();
  scene.add(helper);

  // Loading the first molecule Testetrone on to the scence
  loadProtein( MOLECULES[Object.keys(MOLECULES)[0]] );

  window.addEventListener( 'resize', onWindowResize, false );

  return true;

}

/** Event listener to resize the renererobject, render target and camera accordingly.
    Documentation and Internet
*/
function onWindowResize( event ) {

  var width = window.innerWidth;
  var height = window.innerHeight;

  camera.aspect = width / height;
  camera.updateProjectionMatrix();

  renererobject.setSize( width, height );

  var pixelRatio = renererobject.getPixelRatio();
  var newWidth  = Math.floor( width * pixelRatio ) || 1;
  var newHeight = Math.floor( height * pixelRatio ) || 1;
  depthRenderTarget.setSize( newWidth, newHeight );

}

/**
  Loads a molecule and calls functions to calculate the ambient occlusion map.
*/
function loadProtein( url ) {
  // creating a threejs object and adding root it the scence i.e the molecule
  scene.remove(root);
  root = new THREE.Group();
  scene.add(root);
  // Loading the molecule where load function read the file and reads the xyz and define sphere radius of the atoms 
  loader.load(dir+url, function(json) {
    // Got the json object with the data which has all the atoms x,y,z and radius  and element data
    console.log(json);
    // Gemoetery is object which stores all data, including vertex positions, face indices, normals, colors, UVs, and custom attributes within buffers
    var geometry = new THREE.InstancedBufferGeometry();
    // Creating a plane and copying it to PlaneBuffer
    geometry.copy( new THREE.PlaneBufferGeometry( 1,1,1,1 ) );

    // Finding the length of json object atoms which is the total number of atoms
    atomCount = json.atoms.length;
    // Creating the Array to store the color,index,xyz cordinates array. and colors array
    var translateArray = new Float32Array( atomCount * 3 );
    var scaleArray = new Float32Array( atomCount );
    var colorsArray = new Float32Array( atomCount * 3 );
    var indexArray = new Float32Array( atomCount );
    // a vector 3d
    max = new THREE.Vector3(-Infinity, -Infinity, -Infinity);
    min = new THREE.Vector3(Infinity, Infinity, Infinity);
    avg = new THREE.Vector3();
    // storing the values
    for ( var i = 0, i3 = 0, l = atomCount; i < l; i ++, i3 += 3 ) {

      translateArray[ i3 + 0 ] = json.atoms[i][0];
      max.x = Math.max(max.x, json.atoms[i][0]);
      min.x = Math.min(min.x, json.atoms[i][0]);

      translateArray[ i3 + 1 ] = json.atoms[i][1];
      max.y = Math.max(max.y, json.atoms[i][1]);
      min.y = Math.min(min.y, json.atoms[i][1]);

      translateArray[ i3 + 2 ] = json.atoms[i][2];
      max.z = Math.max(max.z, json.atoms[i][2]);
      min.z = Math.min(min.z, json.atoms[i][2]);

      colorsArray[ i3 + 0 ] = json.atoms[i][3][0]/255;
      colorsArray[ i3 + 1 ] = json.atoms[i][3][1]/255;
      colorsArray[ i3 + 2 ] = json.atoms[i][3][2]/255;

      scaleArray[ i ] = json.atoms[i][4];
      // console.log(json.atoms[i][4])
      indexArray[ i ] = i;
    }

    console.log('No of atoms:'+atomCount);
    avg.lerpVectors(max,min,0.5);
    // setting up the camera vector
    camera.position.set(avg.x,avg.y,max.z*10+10);
    camera.lookAt(avg);
    camera.up.set(0,1,0);
    controls.target = avg;
    // adding new attributes to geomertry object the tranlate array ,color array and shpere radius, color and index array
    geometry.addAttribute( "translate", new THREE.InstancedBufferAttribute( translateArray, 3, 1 ) );
    geometry.addAttribute( "sphereRadius", new THREE.InstancedBufferAttribute( scaleArray, 1, 1 ));
    geometry.addAttribute( "color", new THREE.InstancedBufferAttribute( colorsArray, 3, 1 ) );
    geometry.addAttribute( "impostorIndex", new THREE.InstancedBufferAttribute( indexArray, 1, 1 ) );
    //A mesh is an object that takes a geometry, and applies a material to it.
    var mesh = new THREE.Mesh( geometry, material );
    mesh.frustumCulled = false;
    // Adding the mesh object to the scene
    root.add( mesh );
  },
  function( xhr ){
    if (xhr.lengthComputable) {
      console.log(Math.round( 100*xhr.loaded/xhr.total, 2 )+ '% loaded');
    }
  },
  function( xhr ){
    console.log(Error(xhr),url);
  });
}

/** The rendering loop which is basically rendring the object continously based some frame per second */
function animate() {

  requestAnimationFrame( animate );
  controls.update();
  render();
}

/** The rendering function which used the rendere object to render based on Webgl Renderes */
function render() {
    scene.overrideMaterial = null;
    material.uniforms['invViewMatrix'] = { value: camera.matrixWorld };
    renererobject.render( scene, camera ); 
}

/**
  Initializes the GUI and adds callbacks for change events.
*/
function initGui() {
  var gui = new dat.GUI();
  gui.add(guiParams, 'Molecule', Object.keys(MOLECULES)).onChange(function(v){loadProtein(MOLECULES[v])});
  gui.add(guiParams, 'Saturation', 0.0, 1.0).onChange(function(v){material.uniforms.saturation.value = 1.0 - v});
  gui.add(guiParams, 'cutOff', 0.0, 1.0).onChange(function(v){material.uniforms.cutOff.value = v});
}

/**
  Snippet Got from the net
  Loads a shader based on the type i.e vertex or fragment
*/
function loadShader(shader, type) {
  var $shader = $(shader);
  if(shaderSourceCodes[$shader.attr('title')]===undefined){
    shaderSourceCodes[$shader.attr('title')]={};
  }
  $.ajax({
    url: $shader.attr('src'),
    dataType: 'text',
    context: {
      name: $shader.attr('title'),
      type: type
    },
    complete: function( jqXHR, textStatus ) {
      shadersLoaderCount--;

      shaderSourceCodes[$shader.attr('title')][this.type] = jqXHR.responseText;
      if ( !shadersLoaderCount ) {
        //Calling the shader loading complete function
        shadersLoadComplete();
      }
    }
  });
}


/**
  Starts initialization of the actual application after the shaders have been loaded.
*/
function shadersLoadComplete() {
  console.log(Object.keys(shaderSourceCodes).length + " shaders loaded. Initializing..." );
  if(init()) {
    // Intialing the GUI
    initGui();
    console.log("Initialization succesfull. Start rendering...");
    animate();
  }
}

/**
  Entry point. First loads all the shaders, then init() etc. is called after load completion.
*/
function start() {

  for(var i =0; i<vertexShaders.length;i++){
    loadShader(vertexShaders[i], 'vertex');
  }
  for(var i =0; i<fragmentShaders.length;i++){
    loadShader(fragmentShaders[i], 'fragment');
  }
}

// Start is the function first called which load the the shader one by one
start();
